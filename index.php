<!DOCTYPE html>
<html lang="en">
<head>
<title>Lab 3 SE3316A - Jpate43</title>

<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>

<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

<link href="index.css" rel="stylesheet">
</head>
<body>
    <div class="navbar-wrapper">
        <div class="container">
            
        <div class="navbar navbar-static-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
        
                    <a class="navbar-brand" href="#">LAB 3</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="work.php">Work</a></li>
                        <li><a href="arts.php">Artists</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        </div>
    </div>

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicator -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    
    <?php
    //$paintings = file("data-files/paintings.txt");
    $myfile = fopen("data-files/paintings.txt", "r") or die("Unable to open file!");
    $delimiter = '~';
    
    for($i=0; $i<5; $i++)
    {
        $painting = fgets($myfile);
        $singlePainting = explode($delimiter,$painting);
        
        if($i==0)
            echo '<div class=\'item active\'>';
        else
            echo '<div class=\'item\'>';
        
        echo '<img src=\'';
        echo 'art-images/paintings/medium/'.$singlePainting[3].'.jpg\' ';
        echo 'alt=\''.$singlePainting[4].'\' title=\''.$singlePainting[4];
        echo '\' rel=\"#PaintingThumb/> ';
        
        echo '<div class=\'container\'>';
        echo '<div class=\'carousel-caption\'>';
        echo '<h1>'.$singlePainting[4].'</h1>';
        echo '<p>'.$singlePainting[6].'</p>';
        echo '<p><a class="btn btn-lg btn-primary"';
        echo ' href=\''.$singlePainting[12].'\'';
        echo ' role=\'button\'>Learn more</a></p></div></div></div>';
    }
    ?>
    
    </div>

        <!-- Left and Right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    
    <div class="container marketing">
        
        <?php
        echo '<div class=\'row\'>';
        
        for($i=0; $i<3; $i++)
        {
            $painting = fgets($myfile);
            $singlePainting = explode($delimiter,$painting);
            $firstSentence = explode('.',$singlePainting[5]);
            
            echo '<div class=\'col-lg-4\'>';
            echo '<img class=\'img-circle\' src=\'';
            echo 'art-images/paintings/medium/'.$singlePainting[3].'.jpg\' ';
            echo 'alt=\''.$singlePainting[4].'\' title=\''.$singlePainting[4];
            echo '\' style=\'width:100px; height:100px;\'/> ';
            echo '<h2>'.$singlePainting[4].'</h2>';
            echo '<p class=\'text-justify\'>'.$firstSentence[0];
            echo '</p><p>';
            echo '<a class=\'btn btn-default\' href=\''.$singlePainting[12].'\' role=\'button\'>';
            echo "View Details &raquo";
            echo '</a></p></div>';
        }
        
        echo '</div>';
        echo '<div class=\'row\'>';
        
        for($i=0; $i<3; $i++)
        {
            $painting = fgets($myfile);
            $singlePainting = explode($delimiter,$painting);
            $firstSentence = explode('.',$singlePainting[5]);
            
            echo '<div class=\'col-lg-4\'>';
            echo '<img class=\'img-circle\' src=\'';
            echo 'art-images/paintings/medium/'.$singlePainting[3].'.jpg\' ';
            echo 'alt=\''.$singlePainting[4].'\' title=\''.$singlePainting[4];
            echo '\' style=\'width:100px; height:100px;\'/> ';
            echo '<h2>'.$singlePainting[4].'</h2>';
            echo '<p class=\'text-justify\'>'.$firstSentence[0];
            echo '</p><p>';
            echo '<a href=\''.$singlePainting[12].'\' role=\'button\'>';
            echo "View Details &raquo";
            echo '</a></p></div>';
        }
        
        echo '</div>';
        ?>
        
    </div>

<!-- Add JQuery from Google -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Add Bootstrap from resource files -->
<script src="bootstrap/js/bootstrap.min.js"></script>

</body>
</html>