<!DOCTYPE html>
<html lang="en">
<head>
<title>Lab 3 SE3316A - Jpate43</title>
<!-- Include Google Font -->
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>
<!-- Bootstrap stylesheet -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link href="stylesheet.css" rel="stylesheet">
</head>
<body>
    <header>
        <div id="topHeaderRow">
            <div class="container">
                <!-- Create the nav bar -->
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavBar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
                
                    <div class="collapse navbar-collapse pull-right" id="myNavBar">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                        </ul>
                    </div>
                </nav>
            </div>
       </div>
       
        <!-- Create a row for Art Store and Search bar -->
        <div id="artRow">
            <div class="container">
                <div class="row">
                    <!-- Art Store Logo -->
                    <div class="col-md-8">
                        <h1>Art Store</h1>
                    </div>
                    <!-- Search bar -->
                    <div class="col-md-4">
                        <form class="form-inline" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="search">Search</label>
                                <input type="text" class="form-control" placeholder="Search" name="search">
                                <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
       
       <!-- Create the main navigation row for the Site -->
        <div id="navRow">
        <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="mainNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li class="active"><a href="about.php">About Us</a></li>
                            <li><a href="work.php">Art Works</a></li> 
                            <li><a href="artists.php">Artists</a></li>
                            <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Special 1</a></li>
                                    <li><a href="#">Special 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
               </nav>
           </div>
           </div>
       
    </header>
    
    <!-- Create Jumbotron for the about page -->
    <div class="container">
        <div class="jumbotron">
            <h2>About Us</h2>
            <p>This assiggnment was created by:</p><br/>
            <p>Jugal Patel (jpate43@uwo.ca)</p><br/>
            <p>This assignment was created at Western University.</p>
            <a href="https://owl.uwo.ca/portal" class="btn btn-info btn-lg">Learn more</a>
        </div>
    </div>
    
    <!-- Add JQuery from Google -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Add Bootstrap from resource files -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>