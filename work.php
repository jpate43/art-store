<!DOCTYPE html>
<html lang="en">
<head>
<title>Lab 3 SE3316A - Jpate43</title>
<!-- Include Google Font -->
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>
<!-- Bootstrap stylesheet -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link href="stylesheet.css" rel="stylesheet">

</head>
<body>
    <header>
        <div id="topHeaderRow">
            <div class="container">
                <!-- Create the nav bar -->
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavBar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
                
                    <div class="collapse navbar-collapse pull-right" id="myNavBar">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                        </ul>
                    </div>
                </nav>
            </div>
       </div>
       
        <!-- Create a row for Art Store and Search bar -->
        <div id="artRow">
            <div class="container">
                <div class="row">
                    <!-- Art Store Logo -->
                    <div class="col-md-8">
                        <h1>Art Store</h1>
                    </div>
                    <!-- Search bar -->
                    <div class="col-md-4">
                        <form class="form-inline" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="search">Search</label>
                                <input type="text" class="form-control" placeholder="Search" name="search">
                                <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
       
       <!-- Create the main navigation row for the Site -->
        <div id="navRow">
        <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="mainNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li class="active"><a href="work.php">Art Works</a></li> 
                            <li><a href="artists.php">Artists</a></li>
                            <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Special 1</a></li>
                                    <li><a href="#">Special 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
               </nav>
           </div>
           </div>
       
    </header>
    
    <div class="container">
        <div class="row">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $paintingFile = file("data-files/paintings.txt") or die("Unable to open file!");
                
                $delimiter = '~';
                $first = TRUE;
                foreach($paintingFile as $painting)
                {
                    $paintingFields = explode($delimiter, $painting);
                    //Set the first painting as active
                    if($first)
                    {
                        echo '<div class=\'item active\'>';
                    }
                    else
                    {
                        echo '<div class=\'item\'>';
                    }
                    echo '<div class=\'container\'>';
                    echo '<div class=\'col-md-10\'>';
                    echo '<h2>'.$paintingFields[4].'</h2> ';
                    echo '<p><a href=\'#\'>'.$paintingFields[6].'</a></p>';
                    echo '<div class=\'row\'>';
                    echo '<div class=\'col-md-5\'><img class="img-thumbnail img-responsive"';
                    echo 'src=\'art-images/paintings/medium/'.$paintingFields[3].'.jpg\' ';
                    echo 'alt=\''.$paintingFields[4].'\' title=\''.$paintingFields[4].'\'/></div> ';
                    echo '<div class=\'col-md-7\'>';
                    echo '<p style=\'text-align: justify; text-justify: inter-word;\'>';
                    echo mb_strimwidth(strip_tags($paintingFields[5]), 0, 350, "..");
                    echo '</p> ';
                    echo '<p class=\'price\'>'.$paintingFields[11].'</p>';
                    echo '<div class=\'btn-group btn-group-lg\'><button class=\'btn btn-default\' type=\'button\'>';
                    echo '<a href=\'#\'><span class=\'glyphicon glyphicon-gift\'></span> Add to Wish List</a></button>';
                    echo '<button class=\'btn btn-default\' type=\'button\'><a href=\'#\'><span class=\'glyphicon glyphicon-shopping-cart\'>';
                    echo '</span> Add to Shopping Cart</a></button></div>';
                    echo '<p>&nbsp</p>';
                    echo '<div class=\'panel panel-default\'>    <div class=\'panel-heading\'>Product Details</div>   <table class=\'table\'>';
                    echo '<tbody><tr>   <th>Date:</th>    <td>'.$paintingFields[6].'</td>   </tr>';
                    echo '<tr>      <th>Medium:</th>     <td>Oil on canvas</td>    </tr>';
                    echo '<tr>      <th>Dimensions:</th>     <td>'.$paintingFields[7].' x '.$paintingFields[8].'</td>    </tr>';
                    echo '<tr>      <th>Home:</th>     <td><a href=\'#\'>'.$paintingFields[10].'</a></td>    </tr>';
                    echo '<tr>      <th>Link:</th>     <td><a href=\''.$paintingFields[12].'\'>Wiki</a></td>    </tr>';
                    echo '</tbody></table></div>';
                    echo '</div></div></div></div></div>';
                    $first = FALSE;
                }
                
                ?>
            </div>
            <!-- Left and Right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
    </div>

    
    <!-- Add JQuery from Google -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Add Bootstrap from resource files -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>