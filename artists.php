<!DOCTYPE html>
<html lang="en">
<head>
<title>Lab 3 SE3316A - Jpate43</title>
<!-- Include Google Font -->
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>
<!-- Bootstrap stylesheet -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link href="stylesheet.css" rel="stylesheet">
</head>
<body>
    <header>
        <div id="topHeaderRow">
            <div class="container">
                <!-- Create the nav bar -->
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavBar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
                
                    <div class="collapse navbar-collapse pull-right" id="myNavBar">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                        </ul>
                    </div>
                </nav>
            </div>
       </div>
       
        <!-- Create a row for Art Store and Search bar -->
        <div id="artRow">
            <div class="container">
                <div class="row">
                    <!-- Art Store Logo -->
                    <div class="col-md-8">
                        <h1>Art Store</h1>
                    </div>
                    <!-- Search bar -->
                    <div class="col-md-4">
                        <form class="form-inline" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="search">Search</label>
                                <input type="text" class="form-control" placeholder="Search" name="search">
                                <span class="input-group-btn">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
       
       <!-- Create the main navigation row for the Site -->
        <div id="navRow">
        <div class="container">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="mainNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="work.php">Art Works</a></li> 
                            <li class="active"><a href="artists.php">Artists</a></li>
                            <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Specials<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Special 1</a></li>
                                    <li><a href="#">Special 2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
               </nav>
           </div>
           </div>
       
    </header>
    
    <div class="container">
        <h2>This Week's Best Artists</h2>
        <div class='alert alert-warning' role='alert'>Each week we show you who are our best artists ...</div>
        
        <div class='row'>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    
                    <?php
                    $myfile = fopen("data-files/artists.txt", "r") or die("Unable to open file!");
                    $delimiter = '~';
    
                    for($i=0; $i<18; $i++)
                    {
                        $artist = fgets($myfile);
                        $singleArtist = explode($delimiter,$artist);
                        
                        if($i==0)
                            echo '<div class=\'item active\'><div class=\'container\'>';
                
                        if($i == 6 || $i == 12)
                            echo '<div class=\'item\'><div class=\'container\'>';
                        
                        echo '<div class=\'col-md-2\'>';
                        echo '<div class=\'thumbnail\'>';
                        echo '<img src=\'art-images/artists/medium/'.$singleArtist[0].'.jpg\' style=\'width:175px; height:175px;\' />';
                        echo '<br/><div class=\'caption\'><h4>'.$singleArtist[1].' '.$singleArtist[2].'</h4>';
                        echo '<p><a class=\'btn btn-info\' role=\'button\' href=\'';
                        echo $singleArtist[7].'\' role=\'button\'>Learn More';
                        echo '</a></p></div></div></div>';
                        
                        if($i == 5 || $i == 11 || $i == 17)
                            echo '</div></div>';
                    }
                    ?>
                </div>
                <!-- Left and Right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        
        <div class='row'>
            <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    
                    <?php
                    $artist = fgets($myfile);
                    $artist = fgets($myfile);
                    for($i=0; $i<18; $i++)
                    {
                        $artist = fgets($myfile);
                        $singleArtist = explode($delimiter,$artist);
                        
                        if($i==0)
                            echo '<div class=\'item active\'><div class=\'container\'>';
                
                        if($i == 6 || $i == 12)
                            echo '<div class=\'item\'><div class=\'container\'>';
                        
                        echo '<div class=\'col-md-2\'>';
                        echo '<div class=\'thumbnail\'>';
                        echo '<img src=\'art-images/artists/medium/'.$singleArtist[0].'.jpg\' style=\'width:175px; height:175px;\' />';
                        echo '<br/><div class=\'caption\'><h4>'.$singleArtist[1].' '.$singleArtist[2].'</h4>';
                        echo '<p><a class=\'btn btn-info\' role=\'button\' href=';
                        echo $singleArtist[7].'role=\'button\'>Learn More';
                        echo '</a></p></div></div></div>';
                        
                        if($i == 5 || $i == 11 || $i == 17)
                            echo '</div></div>';
                    }
                    ?>
                </div>
                <!-- Left and Right controls -->
                <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <h4>Artists by Genre</h4>
        <div class="progress">
            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width:7%">
                Gothic
            </div>
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width:27%">
                Renaissance
            </div>
            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" style="width:15%">
                Baroque
            </div>
            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width:21%">
                Pre-Modern
            </div>
            <div class="progress-bar progress-bar-striped" role="progressbar" style="width:30%">
                Modern
            </div>
        </div>
        
    </div>

    
    <!-- Add JQuery from Google -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Add Bootstrap from resource files -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
</body>
</html>